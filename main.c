#include "define.h"
#include "USART.h"
int main(void)
{
	
	delay_init();
	config_gpio();
	pwm_init();
	SetSystem72Mhz();
	USART_Config();
	USARTx_DMA_Config();
	USART_DMACmd(DEBUG_USARTx,USART_DMAReq_Tx,ENABLE);
	sendBuf[15]='\r';
	sendBuf[16]='\n';
	
	while(1)
	{
		run_pwm();
	}
}