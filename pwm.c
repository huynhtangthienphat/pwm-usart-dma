#include "define.h"
#include "USART.h"
int TIM_Pulse = 0;
char buffer[17];
void SetSystem72Mhz(void)
{
	// NHAN 9 VOI TAN SO 
	RCC->CFGR |= (7<<18);
	// CHIA CHO 1.5 
	RCC->CFGR &= ~ (1<<22);
	// SET HSEON 
	RCC->CR |= (1<<16);
	// WAIT HSERDY 
	while((RCC->CR & RCC_CR_HSERDY)==0);
	// PLL FOR MODE HSE 
	RCC->CFGR |= (1<<16);
	
	// TUONG TU VOI HSE TA KICH HOAT PLLON VA CHO NO READY
	RCC->CR |= RCC_CR_PLLON;
	while((RCC->CR & RCC_CR_PLLRDY)==0);
	//Flash pre-fetch enable and wait-state=2
	  //0WS: 0-24MHz
	  //1WS: 24-48MHz
	  //2WS: 48-72MHz
	FLASH->ACR |= FLASH_ACR_PRFTBE|FLASH_ACR_LATENCY_1;
	 //Select PLL as main System Clock source
	RCC->CFGR &= ~ RCC_CFGR_SW;
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	//Wait until PLL system source is active
	while((RCC->CFGR & RCC_CFGR_SWS)!=RCC_CFGR_SWS_PLL);
	
	// AHB DIV1
	RCC->CFGR |= RCC_CFGR_HPRE_DIV1;
	// APB1 DIV 2
	RCC->CFGR |= RCC_CFGR_PPRE1_DIV2;
	// APP2 DIV 1
	RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;
	// ADC PSC = 6 , 2X6 = 12 
	RCC->CFGR |= RCC_CFGR_ADCPRE_DIV6;
	
	// khong anh xa lai ma pin CHO TIM2 
	AFIO->MAPR |= AFIO_MAPR_TIM2_REMAP_NOREMAP;
	
}
void config_gpio(void)
{
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOB,&GPIO_InitStructure);
	
}
void pwm_init(void)
{
	
	// tao xung 100hz 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	
	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure);
	TIM_TimeBaseInitStructure.TIM_Prescaler=720-1;
	TIM_TimeBaseInitStructure.TIM_Period=PERIOD-1;
	TIM_TimeBaseInitStructure.TIM_ClockDivision=0;
	TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4,&TIM_TimeBaseInitStructure);
	
	TIM_OCStructInit(&TIM_OCInitStructure);//PWM
	TIM_OCInitStructure.TIM_OCMode=TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState=TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse=1000;
	TIM_OCInitStructure.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OC1Init(TIM4,&TIM_OCInitStructure); // channel 1;
	TIM_Cmd(TIM4,ENABLE);
}
void run_pwm(void)
{
	for(int i=0;i<PERIOD;i++)
	{
		TIM_Pulse++;
		TIM4->CCR1=TIM_Pulse;
		sprintf(buffer,"TIM Pulse :%d",TIM_Pulse);
		int a=0;
		while(buffer[a]!='\0')
		{
			sendBuf[a]=buffer[a];
			a++;
		}
		USARTx_DMA_Restart();
		DelayMs(10);
	}
	for(int j=PERIOD;j>0;j--)
	{
		TIM_Pulse--;
		TIM4->CCR1=TIM_Pulse;
		sprintf(buffer,"TIM Pulse :%d",TIM_Pulse);
		int b=0;
		while(buffer[b]!='\0')
		{
			sendBuf[b]=buffer[b];
			b++;
		}
		USARTx_DMA_Restart();
		DelayMs(10);
	}
}