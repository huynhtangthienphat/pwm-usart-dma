#include "stm32f10x.h"                  // Device header
#include "stm32f10x_gpio.h"             // Keil::Device:StdPeriph Drivers:GPIO
#include "stm32f10x_rcc.h"              // Keil::Device:StdPeriph Drivers:RCC
#include "stm32f10x_tim.h"              // Keil::Device:StdPeriph Drivers:TIM
#include "stm32f10x_usart.h"            // Keil::Device:StdPeriph Drivers:USART
#include "stm32f10x_dma.h"              // Keil::Device:StdPeriph Drivers:DMA

#include "delay.h"
#define PERIOD 1000 
void config_gpio(void);
void pwm_init(void);
void run_pwm(void);
void SetSystem72Mhz(void);
