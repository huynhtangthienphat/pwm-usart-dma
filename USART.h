#include "define.h"
#include <stdio.h>

#define DEBUG_USARTx USART1
#define DEBUG_USART_CLK RCC_APB2Periph_USART1
#define DEBUG_USART_APBxClkCmd RCC_APB2PeriphClockCmd
#define DEBUG_USART_BAUDRATE 115200

#define DEBUG_USART_GPIO_CLK RCC_APB2Periph_GPIOA
#define DEBUG_USART_GPIO_APBxClkCmd RCC_APB2PeriphClockCmd

#define DEBUG_USART_TX_GPIO_PORT GPIOA
#define DEBUG_USART_TX_GPIO_PIN GPIO_Pin_9
#define DEBUG_USART_RX_GPIO_PORT GPIOA
#define DEBUG_USART_RX_GPIO_PIN GPIO_Pin_10

#define USART_TX_DMA_CHANNEL DMA1_Channel4 // see picture in desktop 
#define USART_DR_ADDRESS ((u32)&USART1->DR)
#define SENDBUFF_SIZE 17
extern uint8_t sendBuf[SENDBUFF_SIZE];
void USARTx_DMA_Config(void);
void USARTx_DMA_Restart(void);

void USART_Config(void);
